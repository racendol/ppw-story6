from django import forms

class StatusForm(forms.Form):
    attrs = {
        'type':'text',
        'class': 'form-control w-50 offset-3',
        'id':'status_form'
    }

    status = forms.CharField(label = "Apa statusmu hari ini?", max_length=300, required=True, widget=forms.TextInput(attrs=attrs))

