from django.test import TestCase, Client
from django.urls import resolve
from .views import index, delete_status
from .models import Status
from .forms import StatusForm

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

# Create your tests here.

class UnitTest(TestCase):
    def test_if_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_if_page_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    
    def test_if_url_exist_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_if_index_used_the_right_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_if_delete_status_exist(self):
        response = Client().get('/delete_status/')
        self.assertEqual(response.status_code, 302)

    def test_komentar_in_database(self):
        Status.objects.create(status = 'tes')
        self.assertEqual(Status.objects.count(), 1)
        
        status = Status.objects.get(id=1)
        self.assertEqual(status.status, 'tes')

    def test_form_is_valid(self):
        form = StatusForm({'status':'tes'})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['status'], 'tes')
    
    def test_form_is_not_valid(self):
        form = StatusForm({})
        self.assertFalse(form.is_valid())
    
    
    
class FunctionalTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = Options()
        options.headless = True
        cls.selenium = webdriver.Firefox(options=options)
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_input_form(self):
        self.selenium.get(self.live_server_url)
        input_form = self.selenium.find_element_by_id('status_form')

        input_form.send_keys('Coba Coba')
        self.selenium.find_element_by_id('simpan').click()
        self.assertIn('Coba Coba', self.selenium.page_source)

        self.selenium.find_element_by_id('hapus').click()
        self.assertNotIn('Coba Coba', self.selenium.page_source)
        self.assertIn('Belum ada status', self.selenium.page_source)