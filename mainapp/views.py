from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.
def index(request):
    response = {'status_form':StatusForm}
    if(request.method == "POST"):
        form = StatusForm(request.POST)
        if(form.is_valid()):
            isi_status = form.cleaned_data['status']
            status = Status(status=isi_status)
            status.save()

    statuses = Status.objects.all()
    response['statuses'] = statuses
    return render(request, 'index.html', response)

def delete_status(request):
    if(request.method == "POST"):
        status_id = request.POST.get('id', '')
        status = Status.objects.get(id=status_id)
        status.delete()
    
    return redirect('index')